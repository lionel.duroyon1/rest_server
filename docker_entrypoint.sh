#!/bin/bash

# Collect static files
echo "Collect static files"
python rest_aurant/manage.py collectstatic --noinput

# Create migrations
echo "Creating migrations"
python rest_aurant/manage.py makemigrations

# Apply database migrations
echo "Apply database migrations"
python rest_aurant/manage.py migrate

#chmod 777 rest_aurant/db.sqlite3
# Create admin user
python rest_aurant/manage.py shell -c "from django.contrib.auth.models import User; User.objects.create_superuser('admin', 'admin@example.com', 'pass')"

# Start server
echo "Starting server"
python rest_aurant/manage.py runserver 0.0.0.0:8000
