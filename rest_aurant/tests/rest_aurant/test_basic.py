"""
Rest_aurant test module
"""
import json
import pytest

POST_CREATED = 201
GET_SUCCESS = 200
BASE_URL = "http://localhost:8010/rest_aurant/"


@pytest.mark.django_db
def test_post_get(admin_client):
    data = {"name": "resto1", "location": "paris"}

    # Checking successful Post
    post_res = admin_client.post(BASE_URL, data)
    assert post_res.status_code == POST_CREATED

    # Check successful Get all
    get_res = admin_client.get(BASE_URL)
    assert get_res.status_code == GET_SUCCESS
    print(get_res.content)
    assert json.loads(get_res.content)[0]["name"] == "resto1"
    assert json.loads(get_res.content)[0]["location"] == "paris"

    # Check successful get one
    get_res = admin_client.get(BASE_URL + "1/")
    assert get_res.status_code == GET_SUCCESS
    print(get_res.content)
    assert json.loads(get_res.content)["name"] == "resto1"
    assert json.loads(get_res.content)["location"] == "paris"

def test_get_random(admin_client):
    data = {"name": "resto2", "location": "toulouse"}

    # Checking successful Post
    post_res = admin_client.post(BASE_URL, data)
    assert post_res.status_code == POST_CREATED

    get_random_res = admin_client.get(BASE_URL + "random/")
    assert get_random_res.status_code == GET_SUCCESS
    print(get_random_res.content)
    assert len(get_random_res.content) > 0
