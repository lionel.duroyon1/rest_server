from rest_framework import serializers

from core.models import Restaurant


class RestaurantSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(read_only=True)

    class Meta:
        model = Restaurant
        fields = ['id', 'name', 'location']
