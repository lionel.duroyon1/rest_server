import random

from rest_framework import permissions, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from core.models import Restaurant
from core.serializers import RestaurantSerializer


class RestaurantViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Restaurant.objects.all().order_by('name')
    serializer_class = RestaurantSerializer
    permission_classes = [
        permissions.AllowAny
    ]

    @action(detail=False, methods=['get'])
    def random(self, request):
        random_resto = random.choice(Restaurant.objects.all())
        return Response(RestaurantSerializer(random_resto).data)
