# REST_aurant

### Version : 0.1

Rest api server for restaurants

# API documentation

http://localhost:8010/docs/

# Installation Docker part ( server part )

Build docker image
```
sudo ./build_docker.sh
```

Launch docker image
```
sudo ./launch_docker.sh
```

Please connect to : http://localhost:8010/


# Installation Local Part

Install local python requirements in your python3 virtualenv :
```
pip3 install -r local_requirements.txt
```

Launch tests :
 ```
 pytest rest_aurant/tests/ -s
```
